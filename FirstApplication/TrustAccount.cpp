#include "TrustAccount.h"

TrustAccount::TrustAccount(string InName, double InBalance, double InInterest)
	: SavingsAccount{InName, InBalance, InInterest}, Withdrawals{0} {}

bool TrustAccount::Deposit(double InAmount)
{
	if (InAmount >= DEFAULT_MIN_VALUE_FOR_BONUS_DEPOSIT) { InAmount += DEFAULT_BONUS_DEPOSIT; }

	return SavingsAccount::Deposit(InAmount);
}

bool TrustAccount::Withdraw(double InAmount)
{
	if (Withdrawals >= DEFAULT_MAX_WITHDRAWALS || InAmount > (Balance * DEFAULT_MAX_WITHDRAWAL_PERCENT)) { return false; }
	else 
	{
		++Withdrawals;
		return SavingsAccount::Withdraw(InAmount);
	}
}

void TrustAccount::Print(ostream& Os) const
{
	Os << "[ Checking account: " << Name << " : " << Balance << " , " << InterestRate << "% , withdrawals: " << Withdrawals << " ]";
}