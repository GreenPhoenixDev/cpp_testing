// FirstApplication.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#pragma once

#include <iostream>
#include <iterator>
#include <iomanip>
#include <algorithm>
#include <deque>
#include <list>
#include <stack>
#include <queue>
#include <fstream>
#include <sstream>
#include <memory>
#include <vector>
#include <set>
#include <map>
#include <list>
#include <string>
#include <ctype.h>
#include "Player.h"
#include "Movies.h"
#include "MyString.h"
#include "Account.h"
#include "SavingsAccount.h"
#include "CheckingAccount.h"
#include "TrustAccount.h"
#include "AccountUtility.h"
#include "Circle.h"
#include "Line.h"
#include "Square.h"
#include "Test.h"
#include "ExeptionClasses.h"
#include "Item.h"
#include "MyPair.h"
#include "Array.h"
#include "Song.h"

#pragma region std::
using std::cout;
using std::cerr;
using std::cin;
using std::find;
using std::fstream;
using std::ifstream;
using std::ofstream;
using std::stringstream;
using std::istringstream;
using std::ostringstream;
using std::numeric_limits;
using std::streamsize;
using std::toupper;
using std::transform;
using std::endl;
using std::setw;
using std::getline;
using std::boolalpha;
using std::fixed;
using std::left;
using std::right;
using std::setfill;
using std::setprecision;

using std::array;
using std::deque;
using std::vector;
using std::string;
using std::list;
using std::map;
using std::set;
using std::stack;
using std::queue;

using std::unique_ptr;
using std::make_unique;
using std::move;
using std::shared_ptr;
using std::make_shared;
using std::weak_ptr;
#pragma endregion

# pragma region Functions
/*
void DisplayMenu()
{
	cout << endl;
	cout << "P - Print numbers" << endl;
	cout << "A - Add a number" << endl;
	cout << "M - Display mean/average of the numbers" << endl;
	cout << "S - Display the smallest number" << endl;
	cout << "L - Display the largest number" << endl;
	cout << "Q - Quit" << endl;
	cout << endl;
}
void PrintNumbers(vector<int>& arr)
{
	if (arr.size() > 0)
	{
		for (int Number : arr)
		{
			cout << Number << " ";
		}
	}
	else
	{
		cout << "[] - the list is empty";
	}
}
void AddNumber(vector<int>& arr)
{
	int NewNumber{ 0 };
	cout << "Please enter an integer: ";
	cin >> NewNumber;
	arr.push_back(NewNumber);
	cout << endl;
	cout << "Successfully added: " << NewNumber;
}
void DisplayAverage(vector<int>& arr)
{
	if (arr.size() > 0)
	{
		double Sum{ 0 };
		for (int Number : arr)
		{
			Sum += Number;
		}
		double Average{ 0.0f };
		Average = Sum / arr.size();
		cout << "The mean/average is: " << Average;
	}
	else
	{
		cout << "Unable to calculate the mean/average - the list is empty";
	}
}
void DisplaySmallestNumber(vector<int>& arr)
{
	if (arr.size() > 0)
	{
		int CurrentSmallest{ INT_MAX };
		for (int Number : arr)
		{
			if (Number < CurrentSmallest)
				CurrentSmallest = Number;
		}
		cout << "The smallest number is: " << CurrentSmallest;
	}
	else
	{
		cout << "Unable to determine the smallest number - the list is empty";
	}
}
void DisplayLargestNumber(vector<int>& arr)
{
	if (arr.size() > 0)
	{
		int CurrentLargest{ 0 };
		for (int Number : arr)
		{
			if (Number > CurrentLargest)
				CurrentLargest = Number;
		}
		cout << "The largest number is: " << CurrentLargest;
	}
	else
	{
		cout << "Unable to determine the largest number - the list is empty";
	}
}
void Quit(bool& bAllowedToQuit)
{
	bAllowedToQuit = true;
	cout << "Goodbye";
}
*/
//vector<int>* ApplyAll(const vector<int>* Vector1, const vector<int>* Vector2)
//{
//	vector<int>* ResultingVector = new vector<int>{};
//
//	for (size_t i = 0; i < Vector1->size(); i++)
//	{
//		for (size_t j = 0; j < Vector2->size(); j++)
//		{
//			int Product = Vector1->at(i)* Vector2->at(j);
//			ResultingVector->push_back(Product);
//		}
//	}
//
//	return ResultingVector;
//}
//void Print(const vector<int>* Vector)
//{
//	for (size_t i = 0; i < Vector->size(); i++)
//	{
//		cout << Vector->at(i) << " ";
//	}
//	cout << endl;
//	cout << endl;
//}
/*
	Movies* MoviesObject = new Movies();
	while (MoviesObject->bIsRunning)
	{
		MoviesObject->DefaultUpdate();
	}

	delete MoviesObject;
*/
/*
void DisplayMenu();
void PrintNumbers(vector<int>& arr);
void AddNumber(vector<int>& arr);
void DisplayAverage(vector<int>& arr);
void DisplaySmallestNumber(vector<int>& arr);
void DisplayLargestNumber(vector<int>& arr);
void Quit(bool& bAllowedToQuit);
*/
//vector<int>* ApplyAll(const vector<int>* Vector1, const vector<int>* Vector2);
//void Print(const vector<int>* Vector);
//void ScreenRefresh(const vector<Shape*>& Shapes)
//{
//	cout << "Refreshing" << endl;
//	for (const auto S : Shapes)
//	{
//		S->Draw();
//	}
//}
/*
class BClass;

class AClass
{
	shared_ptr<BClass> BPtr;
public:
	//Constructors
	AClass() { cout << "A Constructor" << endl; }
	~AClass() { cout << "A Destructor" << endl; }

	void SetB(shared_ptr<BClass>& InB) { BPtr = InB; }
};

class BClass
{
	weak_ptr<AClass> APtr;
public:
	//Constructors
	BClass() { cout << "B Constructor" << endl; }
	~BClass() { cout << "B Destructor" << endl; }

	void SetA(shared_ptr<AClass>& InA) { APtr = InA; }
};
*/
/*
unique_ptr<vector<shared_ptr<Test>>> Make()
{
	return make_unique<vector<shared_ptr<Test>>>();
}

void Fill(vector<shared_ptr<Test>>& Vec, int InNum)
{
	for (size_t i = 1; i <= InNum; i++)
	{
		cout << "Enter data point [" << i << "] : ";
		int Input;
		cin >> Input;
		shared_ptr<Test> SPtr = make_shared<Test>(Input);
		Vec.push_back(SPtr);
	}
}

void Display(vector<shared_ptr<Test>>& Vec)
{
	cout << "Displaying Vector Data" << endl;
	cout << "======================" << endl;
	for (auto T : Vec)
	{
		cout << T->GetData() << endl;
	}
}
*/
//double CalculateLpK(int InKilometers, int InLiters)
//{
//	if (InLiters == 0)
//		throw DividedByZeroExeption();
//
//	if (InKilometers < 0 || InLiters < 0)
//		throw NegativeValueExeption();
//
//	return static_cast<double>(InKilometers) / InLiters;
//}
/*
struct City
{
	string Name;
	long Population;
	double Cost;
};

struct Country
{
	string Name;
	vector<City> Cities;
};

struct Tour
{
	string Title;
	vector<Country> Countries;
};

void Ruler() { cout << "\n1234567890123456789012345678901234567890123456789012345678901234567890\n" << endl; }
*/
/*
string CorrectAnswers;

int CompareAnswers(string& InAnswers)
{
	int RightAnswers{ 0 };
	for (size_t i = 0; i < CorrectAnswers.size(); i++)
	{
		if (CorrectAnswers.at(i) == InAnswers.at(i))
			RightAnswers++;
	}
	return RightAnswers;
}
*/
/*
vector<string> Lines;
vector<string> UpperLines;
bool bSearchAllUpper;

void FillWords(ifstream& InStream)
{
	string Line{};
	int Count{ 0 };

	while (getline(InStream, Line))
	{
		if (Line == "")
			continue;

		Lines.push_back(Line);

		string UpperLine{ Line };
		transform(UpperLine.begin(), UpperLine.end(), UpperLine.begin(), toupper);
		UpperLines.push_back(UpperLine);
	}


	cout << "There are " << Lines.size() << " lines in the vector." << endl;
}

bool AskForCapitalisation()
{
	string YesNo{};
	cout << endl;
	cout << "Do you want to ignore capitalisation for the search? ( Yes / No ): ";
	cin >> YesNo;
	transform(YesNo.begin(), YesNo.end(), YesNo.begin(), toupper);

	if (YesNo == "YES")
		return true;
	else if (YesNo == "NO")
		return false;
	else
		cout << "Invalid Input, please try again... " << endl;

	cout << "Search completed!" << endl;
	return AskForCapitalisation();
}

int SearchForWord(const string& InWord, const vector<string>& InVector)
{
	cout << "Searching " << InVector.size() << " lines..." << endl;

	int WordCount{ 0 };

	// COMPARE EVERY WORD IN THE ARRAY
	for (size_t i = 0; i < InVector.size(); i++)
	{
		// if the InWord is bigger than the the word to check against, just skip it
		if (InWord.size() > InVector.at(i).size())
			continue;

		string CurrentWord{ InVector.at(i) };
		string Word{};
		int k{ 0 };

		// create a substring of the length of the InWord and compare it to the InWord
		int Size = CurrentWord.size() - InWord.size();
		for (size_t j = 0; j < Size; j++)
		{
			string SubString{};
			string Copy{ CurrentWord };
			SubString = Copy.substr(j, InWord.size());

			if (InWord == SubString)
				WordCount++;
		}
	}

	return WordCount;
}
*/
//template <typename T>
//T Max(T A, T B)
//{
//	return (A > B) ? A : B;
//}
/*
void Display(const vector<int>& InVector)
{
	cout << "[ ";
	for (const auto& I : InVector)
	{
		cout << I << " ";
	}
	cout << "]" << endl;
}
void Test1()
{
	cout << "\n=============================================" << endl;

	vector<int> Nums{ 1,2,3,4,5 };
	auto It = Nums.begin();
	cout << *It << endl;


	It++;
	cout << *It << endl;

	It += 2;
	cout << *It << endl;

	It -= 2;
	cout << *It << endl;

	It = Nums.end() - 1;
	cout << *It << endl;
}
void Test2()
{
	cout << "\n=============================================" << endl;

	vector<int> Nums{ 1,2,3,4,5 };
	vector<int>::iterator It = Nums.begin();
	while (It != Nums.end())
	{
		cout << *It << endl;
		It++;
	}

	It = Nums.begin();
	while (It != Nums.end())
	{
		*It = 0;
		It++;
	}

	Display(Nums);
}
void Test3()
{
	// constant iterators: unable to change iterator values
	cout << "\n=============================================" << endl;

	vector<int> Nums{ 1,2,3,4,5 };
	vector<int>::const_iterator It = Nums.begin(); // auto It = Nums.cbegin();
	while (It != Nums.end())
	{
		cout << *It << endl;
		It++;
	}
}
void Test4()
{
	cout << "\n=============================================" << endl;

	// reverse iterators
	vector<int> Vector{ 1,2,3,4,5 };
	auto It1 = Vector.rbegin();
	while (It1 != Vector.rend())
	{
		cout << *It1 << endl;
		It1++;
	}

	list<string> Names{ "Henry", "Baldur", "Bernie", "Wilma", "Otto" };
	auto It2 = Names.rbegin();
	while (It2 != Names.rend())
	{
		cout << *It2 << endl;
		It2++;
	}

	map<string, string> Favorites{ {"Henry", "Blueberry"}, {"Baldur", "Strawberry"}, {"Bernie", "Bromberry"}, {"Wilma", "Cherry"}, {"Otto", "Chocolate"} };
	auto It3 = Favorites.rbegin();
	while (It3 != Favorites.rend())
	{
		cout << It3->first  << " : " << It3->second << endl;
		It3++;
	}
}
void Test5()
{
	cout << "\n=============================================" << endl;

	vector<int> Vector{ 1,2,3,4,5,6,7,8,9,10 };
	auto Start = Vector.begin();
	auto End = Vector.end();
	while (Start != End)
	{
		cout << *Start << endl;
		Start++;
	}
}
*/
/*
const string Alphabet{ "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz" };
const string UpAlphabet{ "ABCDEFGHIJKLMNOPQRSTUVWXYZ" };
const string LowAlphabet{ "abcdefghijklmnopqrstuvwxyz" };

bool IsValidChar(const char& InChar)
{
	if (Alphabet.find(InChar) != string::npos)
	{
		return true;
	}
	else
	{
		return false;
	}
}

char ToUpper(const char& InChar)
{
	if (UpAlphabet.find(InChar) != string::npos)
		return InChar;
	else
		return UpAlphabet.at(LowAlphabet.find(InChar));
}

bool IsPalindrome(const string& InString)
{
	string FormattedString;
	deque<char> Pal;

	// CONVERT STRING TO VALID FORMAT
	for (const auto& Char : InString)
	{
		if (IsValidChar(Char))
		{
			char C = ToUpper(Char);
			FormattedString.push_back(C);
			Pal.push_front(C);
		}
	}
	// CHECK FOR PALINDROME
	for (size_t i = 0; i < FormattedString.size(); i++)
	{
		if (FormattedString.at(i) == Pal.at(i))
			continue;
		else
			return false;
	}

	return true;
}
*/
/*
bool bQuit{ false };

void DisplayMenu()
{
	cout << endl;
	cout << "F - Play First Song" << endl;
	cout << "L - Play Last Song" << endl;
	cout << "N - Play next song" << endl;
	cout << "P - Play previous song" << endl;
	cout << "A - Add and play a new song at current location" << endl;
	cout << "D - Display the current Playlist" << endl;
	cout << "=======================================================================" << endl;
	cout << "Enter a selection (Q to quit): ";
}

void PlayCurrentSong(const Song& InSong)
{
	cout << endl;
	cout << "Current Song: " << endl;
	cout << InSong << endl;
}

void DisplayPlaylist(const list<Song>& InPlaylist, const Song& InCurrentSong)
{
	for (const auto& S : InPlaylist)
	{
		cout << S << endl;
	}

	PlayCurrentSong(InCurrentSong);
}

list<Song>::iterator PlayFirstSong(list<Song>& InPlaylist)
{
	list<Song>::iterator It = InPlaylist.begin();

	cout << "Playing first song" << endl;
	cout << "Playing: " << endl;
	cout << *It << endl;

	return It;
}

list<Song>::iterator PlayLastSong(list<Song>& InPlaylist)
{
	list<Song>::iterator It = InPlaylist.end();
	It--;

	cout << "Playing last song" << endl;
	cout << "Playing: " << endl;
	cout << *It << endl;

	return It;
}

list<Song>::iterator PlayNextSong(list<Song>& InPlaylist, list<Song>::iterator& InCurrentSong)
{
	InCurrentSong++;
	if (InCurrentSong == InPlaylist.end())
		InCurrentSong = InPlaylist.begin();

	cout << "Playing next song" << endl;
	cout << "Playing: " << endl;
	cout << *InCurrentSong << endl;

	return InCurrentSong;
}

list<Song>::iterator PlayPreviousSong(list<Song>& InPlaylist, list<Song>::iterator& InCurrentSong)
{
	if (InCurrentSong == InPlaylist.begin())
		InCurrentSong = InPlaylist.end();

	InCurrentSong--;

	cout << "Playing previous song" << endl;
	cout << "Playing: " << endl;
	cout << *InCurrentSong << endl;

	return InCurrentSong;
}

list<Song>::iterator AddNewSong(list<Song>& InPlaylist, list<Song>::iterator& InCurrentSong)
{
	string NewName;
	string NewArtist;
	int NewRating;
	cin.clear();
	cin.ignore(numeric_limits<streamsize>::max(), '\n');
	cout << "Adding and playing new song" << endl;
	cout << "Enter song name: ";
	getline(cin, NewName);
	cout << "Enter song artist: ";
	getline(cin, NewArtist);
	cout << "Enter your rating(1-10): ";
	cin >> NewRating;
	InPlaylist.insert(InCurrentSong, Song(NewName, NewArtist, NewRating));
	InCurrentSong--;
	PlayCurrentSong(*InCurrentSong);

	return InCurrentSong;
}
*/
/*
void DisplayWords(const map<string, int>& InWordCounts)
{
	cout << endl;
	cout << setw(15) << left << "Word"
		<< setw(10) << right << "Count"
		<< endl;
	cout << "=========================" << endl;

	for (auto Pair : InWordCounts)
	{
		cout << setw(15) << left << Pair.first
			<< setw(10) << right << Pair.second
			<< endl;
	}
}

void DisplayWords(const map<string, set<int>>& InWordOccurences)
{
	cout << endl;
	cout << setw(15) << left << "Word"
		<< "Occurences" << endl;
	cout << "=============================================================================================================================" << endl;

	for (auto Pair : InWordOccurences)
	{
		cout << setw(15) << left << Pair.first
			<< left << "[ ";

		for (auto PairS : Pair.second)
		{
			cout << PairS << " ";
		}

		cout << " ]" << endl;
	}
}

string CleanString(const string& InString)
{
	string Result;
	for (char C : InString)
	{
		if (C == '.' || C == ':' || C == ',' || C == ';')
			continue;
		else
			Result += C;
	}

	return Result;
}

void DisplayWordCounts()
{
	map<string, int> WordCounts;
	string Line;
	string Word;
	ifstream InFile{ "WizardofOz.txt" };
	if (InFile)
	{
		while (getline(InFile, Line))
		{
			stringstream Ss(Line);
			while (Ss >> Word)
			{
				Word = CleanString(Word);
				WordCounts[Word]++;
			}
		}

		InFile.close();
		DisplayWords(WordCounts);
	}
	else
		cerr << "Error opening input file" << endl;
}

void DisplayWordOccurences()
{
	map<string, set<int>> WordOccurences;
	string Line;
	string Word;
	ifstream InFile{ "WizardofOz.txt" };
	if (InFile)
	{
		int LineNumber{ 0 };
		while (getline(InFile, Line))
		{
			LineNumber++;
			stringstream Ss(Line);
			while (Ss >> Word)
			{
				Word = CleanString(Word);
				WordOccurences[Word].emplace(LineNumber);
			}
		}

		InFile.close();
		DisplayWords(WordOccurences);
	}
	else
		cerr << "Error opening input file" << endl;
}
*/

#pragma endregion

bool IsPalindrome(const string& InString)
{
	stack<char> Forward;
	queue<char> Backward;

	// CONVERT STRING TO VALID FORMAT
	for (const auto& Char : InString)
	{
		if (isalpha(Char))
		{
			Forward.push(toupper(Char));
			Backward.push(toupper(Char));
		}
	}
	// CHECK FOR PALINDROME
	while (!Forward.empty())
	{
		if (Forward.top() != Backward.front())
		{
			return false;
		}

		Forward.pop();
		Backward.pop();
	}

	return true;
}

int main()
{
	vector<string> TestStrings{ "a", "aa", "aba", "abba", "abbcbba", "ab", "abc", "radar", "bob", "ana",
	"avid diva", "Amore Roma","A Toyota's a toyota", "C++", "A Santa at NASA",
	"A man, a plan, a cat, a ham, a yak, a yam, a hat, a canal-Panama!", "This is a palindrome", "palindrome" };

	cout << boolalpha;
	cout << setw(8) << left << "Result" << "String" << endl;

	for (const auto& S : TestStrings)
	{
		cout << setw(8) << left << IsPalindrome(S) << S << endl;
	}

	cout << endl;

	return 0;
}

#pragma region main() bodies
// from older to newer
	/*
	cout << "Hi there, welcome to..." << endl;

	int NumberOfSmallRooms{ 0 };
	cout << "\nHow many small rooms...?" << endl;
	cin >> NumberOfSmallRooms;

	int NumberOfLargeRooms{ 0 };
	cout << "\nHow many large rooms...?" << endl;
	cin >> NumberOfLargeRooms;

	const double PRICE_PER_SMALL_ROOM{ 15.0f };
	const double PRICE_PER_LARGE_ROOM{ 30.0f };
	const double SALES_TAX{ 0.06f };
	const int ESTIMATE_EXPIRY{ 25 };

	cout << "\nEstimate for..." << endl;
	cout << "Number of small rooms: " << NumberOfSmallRooms << endl;
	cout << "Number of large rooms: " << NumberOfLargeRooms << endl;
	cout << "Price per small room: $" << PRICE_PER_SMALL_ROOM << endl;
	cout << "Price per large room: $" << PRICE_PER_LARGE_ROOM << endl;
	cout << "Cost: $"
		<< (PRICE_PER_SMALL_ROOM * NumberOfSmallRooms) +
		(PRICE_PER_LARGE_ROOM * NumberOfLargeRooms)
		<< endl;
	cout << "Tax: $"
		<< (PRICE_PER_SMALL_ROOM * NumberOfSmallRooms * SALES_TAX) +
		(PRICE_PER_LARGE_ROOM * NumberOfLargeRooms * SALES_TAX)
		<< endl;
	cout << "----------------------------------------------" << endl;
	cout << "Total estimate: $"
		<< (PRICE_PER_SMALL_ROOM * NumberOfSmallRooms) +
		(PRICE_PER_LARGE_ROOM * NumberOfLargeRooms) +
		(PRICE_PER_SMALL_ROOM * NumberOfSmallRooms * SALES_TAX) +
		(PRICE_PER_LARGE_ROOM * NumberOfLargeRooms * SALES_TAX)
		<< endl;
	cout << "This estimate is valid for " << ESTIMATE_EXPIRY << " days." << endl;
	*/
	/*
	vector<int> Vector1;
	vector<int> Vector2;

	cout << "==================================================================" << endl;

	cout << "Vector1" << endl;
	cout << endl;
	Vector1.push_back(10);
	Vector1.push_back(20);
	cout << "Index 0: " << Vector1.at(0) << endl;
	cout << "Index 1: " << Vector1.at(1) << endl;
	cout << endl;
	cout << "Vector size is: " << Vector1.size() << endl;
	cout << endl;

	cout << "==================================================================" << endl;

	cout << "Vector2" << endl;
	cout << endl;
	Vector2.push_back(100);
	Vector2.push_back(200);
	cout << "Index 0: " << Vector2.at(0) << endl;
	cout << "Index 1: " << Vector2.at(1) << endl;
	cout << endl;
	cout << "Vector size is: " << Vector2.size() << endl;
	cout << endl;

	cout << "==================================================================" << endl;

	vector<vector<int>> Vector2D;

	cout << "Vector2D" << endl;
	cout << endl;
	Vector2D.push_back(Vector1);
	Vector2D.push_back(Vector2);
	cout << "No Changes" << endl;
	cout << "Index 0: " << Vector2D.at(0).at(0) << " " << Vector2D.at(0).at(1) << endl;
	cout << "Index 1: " << Vector2D.at(1).at(0) << " " << Vector2D.at(1).at(1) << endl;
	cout << endl;
	Vector1.at(0) = 1000;
	cout << "Changes in Vector1" << endl;
	cout << "Index 0: " << Vector2D.at(0).at(0) << " " << Vector2D.at(0).at(1) << endl;
	cout << "Index 1: " << Vector2D.at(1).at(0) << " " << Vector2D.at(1).at(1) << endl;
	cout << endl;

	cout << "==================================================================" << endl;

	cout << "Vector1" << endl;
	cout << endl;
	cout << "Index 0: " << Vector1.at(0) << endl;
	cout << "Index 1: " << Vector1.at(1) << endl;
	cout << endl;
	cout << "Vector size is: " << Vector1.size() << endl;
	cout << endl;
	*/
	/*
	int Int1{ 0 };
	int Int2{ 0 };
	int Int3{ 0 };
	cout << "Please type in 3 integers divided by a space between each of them." << endl;
	cin >> Int1 >> Int2 >> Int3;
	cout << endl;
	cout << "Your 3 integers are: " << Int1 << " " << Int2 << " " << Int3 << endl;;
	cout << endl;
	int Sum{ 0 };
	Sum = Int1 + Int2 + Int3;
	cout << "The sum of your integers is: " << Sum << endl;
	cout << endl;
	double Average{ 0.0f };
	Average = static_cast<double>(Sum) / 3;
	cout << "The average of these integers is: " << Average << endl;
	*/
	/*
	const int EURO_500{ 50000 };
	const int EURO_200{ 20000 };
	const int EURO_100{ 10000 };
	const int EURO_50{ 5000 };
	const int EURO_20{ 2000 };
	const int EURO_10{ 1000 };
	const int EURO_5{ 500 };
	const int EURO_2{ 200 };
	const int EURO_1{ 100 };
	const int CENT_50{ 50 };
	const int CENT_20{ 20 };
	const int CENT_10{ 10 };
	const int CENT_5{ 5 };
	const int CENT_2{ 2 };
	const int CENT_1{ 1 };

	vector<int> PartsOfChange(15, 0);

	cout << "Enter an amount of cent..." << endl;
	int Cents{ 0 };
	cin >> Cents;
	int Balance{ 0 };
	Balance = Cents;

	PartsOfChange.at(0) = Balance / EURO_500;
	Balance %= EURO_500;

	PartsOfChange.at(1) = Balance / EURO_200;
	Balance %= EURO_200;

	PartsOfChange.at(2) = Balance / EURO_100;
	Balance %= EURO_100;

	PartsOfChange.at(3) = Balance / EURO_50;
	Balance %= EURO_50;

	PartsOfChange.at(4) = Balance / EURO_20;
	Balance %= EURO_20;

	PartsOfChange.at(5) = Balance / EURO_10;
	Balance %= EURO_10;

	PartsOfChange.at(6) = Balance / EURO_5;
	Balance %= EURO_5;

	PartsOfChange.at(7) = Balance / EURO_2;
	Balance %= EURO_2;

	PartsOfChange.at(8) = Balance / EURO_1;
	Balance %= EURO_1;

	PartsOfChange.at(9) = Balance / CENT_50;
	Balance %= CENT_50;

	PartsOfChange.at(10) = Balance / CENT_20;
	Balance %= CENT_20;

	PartsOfChange.at(11) = Balance / CENT_10;
	Balance %= CENT_10;

	PartsOfChange.at(12) = Balance / CENT_5;
	Balance %= CENT_5;

	PartsOfChange.at(13) = Balance / CENT_2;
	Balance %= CENT_2;

	PartsOfChange.at(14) = Balance;

	cout << endl;
	cout << "500 Euro: " << PartsOfChange.at(0) << endl;
	cout << "200 Euro: " << PartsOfChange.at(1) << endl;
	cout << "100 Euro: " << PartsOfChange.at(2) << endl;
	cout << "50 Euro: " << PartsOfChange.at(3) << endl;
	cout << "20 Euro: " << PartsOfChange.at(4) << endl;
	cout << "10 Euro: " << PartsOfChange.at(5) << endl;
	cout << "5 Euro: " << PartsOfChange.at(6) << endl;
	cout << "2 Euro: " << PartsOfChange.at(7) << endl;
	cout << "1 Euro: " << PartsOfChange.at(8) << endl;
	cout << "50 Cent: " << PartsOfChange.at(9) << endl;
	cout << "20 Cent: " << PartsOfChange.at(10) << endl;
	cout << "10 Cent: " << PartsOfChange.at(11) << endl;
	cout << "5 Cent: " << PartsOfChange.at(12) << endl;
	cout << "2 Cent: " << PartsOfChange.at(13) << endl;
	cout << "1 Cent: " << PartsOfChange.at(14) << endl;
	*/
	/*
	const string ALPHABET{ "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ" };
	const string CYPHER{ "ZYXWVUTSRQPONMLKJIHGFEDCBAzyxwvutsrqponmlkjihgfedcba" };

	cout << "Please enter the secret message... ";

	string SecretMessage{};
	getline(cin, SecretMessage);
	cout << endl;

	cout << "Encrypting message..." << endl;
	cout << endl;

	// ENCRYPT THE MESSAGE BY CHECKING EVERY LETTER AND REPLACING IT IF POSSIBLE
	string EncryptedMessage{ SecretMessage };
	for (size_t i = 0; i < EncryptedMessage.size(); i++)
	{
		char LetterToReplace = EncryptedMessage.at(i);
		size_t Pos = ALPHABET.find(LetterToReplace);
		if (Pos != string::npos)
		{
			EncryptedMessage.at(i) = CYPHER.at(Pos);
		}
	}

	cout << "This is your encrypted message..." << endl;
	cout << EncryptedMessage << endl;
	cout << endl;

	cout << "Decrypting message..." << endl;
	cout << endl;

	// DECRYPT THE MESSAGE BY CHECKING EVERY LETTER AND REPLACING IT IF POSSIBLE
	string DecryptedMessage{ EncryptedMessage };
	for (size_t i = 0; i < DecryptedMessage.size(); i++)
	{
		char LetterToReplace = DecryptedMessage.at(i);
		size_t Pos = CYPHER.find(LetterToReplace);
		if (Pos != string::npos)
		{
			DecryptedMessage.at(i) = ALPHABET.at(Pos);
		}
	}

	cout << "This is your decrypted message..." << endl;
	cout << DecryptedMessage << endl;
	cout << endl;
	*/
	/*
	// LETTER PYRAMID

	cout << endl;
	cout << "Welcome to the Letter-Pyramid!" << endl;
	cout << endl;
	cout << "Please type anything in to continue... ";
	string InputString{};
	getline(cin, InputString);
	cout << endl;
	cout << endl;

	// DISPLAY PYRAMID
	string OutputString{};
	// i is a placeholder for CurrentLetterCount
	// i always starts at 1 because there is atleast 1 letter needed to display anything
	for (size_t i = 1; i <= InputString.size(); i++)
	{
		string ForwardString{};
		for (size_t j = 0; j < i; j++)
		{
			ForwardString.push_back(InputString.at(j));
		}

		string ReversedString{};
		for (int k = ForwardString.size() - 2; k >= 0; k--)
		{
			ReversedString.push_back(ForwardString.at(k));
		}

		string WhiteSpaces{};
		for (size_t l = 0; l < InputString.size() - i; l++)
		{
			WhiteSpaces.push_back(' ');
		}

		cout << WhiteSpaces + ForwardString + ReversedString << endl;
	}
	*/
	/*
	vector<int> Numbers{};
	bool bQuit{ false };

	do
	{
		char PressedKey{};

		DisplayMenu();

		cin >> PressedKey;
		cout << endl;

		switch (PressedKey)
		{
		case 'p':
		case 'P':
			PrintNumbers(Numbers);
			break;
		case 'a':
		case 'A':
			AddNumber(Numbers);
			break;
		case 'm':
		case 'M':
			DisplayAverage(Numbers);
			break;
		case 's':
		case 'S':
			DisplaySmallestNumber(Numbers);
			break;
		case 'l':
		case 'L':
			DisplayLargestNumber(Numbers);
			break;
		case 'q':
		case 'Q':
			Quit(bQuit);
			break;
		default:
			cout << "Unknown selection, please try again.";
			break;
		}
		cout << endl;
		cout << endl;

	} while (!bQuit);
	*/
	/*
	vector<int>* Vector1 = new vector<int>{ 1, 2, 3, 4, 5 };
	vector<int>* Vector2 = new vector<int>{ 10, 20, 30 };

	cout << "Vector1: ";
	Print(Vector1);

	cout << "Vector2: ";
	Print(Vector2);

	cout << "New Array: ";
	Print(ApplyAll(Vector1, Vector2));

	// Free memory
	// This is ALWAYS needed when variables etc. are created with the - new - keyword
	delete Vector1;
	delete Vector2;
	*/
	/*
	cout << boolalpha << endl;
	string Name1{ "frank" };
	string Name2{ "frank" };
	MyString A{ &Name1 };
	MyString B{ &Name2 };

	cout << (A == B) << endl;
	cout << (A != B) << endl;

	Name2 = "george";
	B = &Name2;

	cout << (A == B) << endl;
	cout << (A != B) << endl;
	*/
	/*
	cout << "================================Account================================" << endl;
	Account Acc{1000.0};
	cout << Acc << endl;
	Acc.Deposit(1000.0);
	cout << Acc << endl;
	Acc.Withdraw(100.0);
	cout << Acc << endl;
	Acc.Withdraw(2000.0);
	cout << Acc << endl;
	cout << endl;

	cout << "============================Savings Account============================" << endl;
	SavingsAccount SAcc{1000.0, 4.0};
	cout << SAcc << endl;
	SAcc.Deposit(1000.0);
	cout << SAcc << endl;
	SAcc.Withdraw(100.0);
	cout << SAcc << endl;
	SAcc.Withdraw(2000.0);
	cout << SAcc << endl;
	cout << endl;
	*/
	/*
	Account A1;
	SavingsAccount A2;
	CheckingAccount A3;
	TrustAccount A4;

	Account& Ref1 = A1;
	Account& Ref2 = A2;
	Account& Ref3 = A3;
	Account& Ref4 = A4;

	DoDebug(Ref1);
	DoDebug(Ref2);
	DoDebug(Ref3);
	DoDebug(Ref4);
	*/
	/*
	Shape* S1 = new Line();
	Shape* S2 = new Circle();
	Shape* S3 = new Square();

	vector<Shape*> Shapes{ S1, S2, S3 };
	ScreenRefresh(Shapes);
	*/
	/*
		cout.precision(2);
		cout << fixed;

		// Savings Accounts
		vector<Account*> SAccounts;
		Account* S1 = new SavingsAccount();
		Account* S2 = new SavingsAccount("S2");
		Account* S3 = new SavingsAccount("S3", 200000.0);
		Account* S4 = new SavingsAccount("S4", 500000.0, 4.0);

		SAccounts.push_back(S1);
		SAccounts.push_back(S2);
		SAccounts.push_back(S3);
		SAccounts.push_back(S4);

		Display(SAccounts);
		Deposit(SAccounts, 500000.0);
		Withdraw(SAccounts, 600000.0);

		// Checking Accounts
		vector<Account*> CAccounts;
		Account* C1 = new CheckingAccount();
		Account* C2 = new CheckingAccount("C2");
		Account* C3 = new CheckingAccount("C3", 200000.0);
		Account* C4 = new CheckingAccount("C4", 500000.0, 4.0);
		CAccounts.push_back(C1);
		CAccounts.push_back(C2);
		CAccounts.push_back(C3);
		CAccounts.push_back(C4);

		Display(CAccounts);
		Deposit(CAccounts, 500000.0);
		Withdraw(CAccounts, 600000.0);

		// Trust Accounts
		vector<Account*> TAccounts;
		Account* T1 = new TrustAccount();
		Account* T2 = new TrustAccount("T2");
		Account* T3 = new TrustAccount("T3", 200000.0);
		Account* T4 = new TrustAccount("T4", 500000.0, 4.0);
		TAccounts.push_back(T1);
		TAccounts.push_back(T2);
		TAccounts.push_back(T3);
		TAccounts.push_back(T4);

		Display(TAccounts);
		Deposit(TAccounts, 500000.0);
		Withdraw(TAccounts, 600000.0);

		for (auto SAcc : SAccounts)
		{
			delete SAcc;
		}
		for (auto CAcc : CAccounts)
		{
			delete CAcc;
		}
		for (auto TAcc : TAccounts)
		{
			delete TAcc;
		}
	*/
	/*
	unique_ptr<Test> T1{ new Test{1000} };
	unique_ptr<Test> T2 = make_unique<Test>(2000);

	unique_ptr<Test> T3;

	T3 = move(T1);
	if (!T1) { cout << "T1 is nullptr" << endl; }

	unique_ptr<Account> A1 = make_unique<CheckingAccount>("General Kenobi", 5000.0);
	cout << *A1 << endl;
	A1->Deposit(5000.0);
	cout << *A1 << endl;

	vector<unique_ptr<Account>> Accounts;
	Accounts.push_back(make_unique<CheckingAccount>("Checkings Account", 2000.0));
	Accounts.push_back(make_unique<SavingsAccount>("Savings Account", 2000.0));
	Accounts.push_back(make_unique<TrustAccount>("Trust Account", 2000.0));

	for (const auto &Acc : Accounts)
	{
		cout << *Acc << endl;
	}
	*/
	/*
	shared_ptr<AClass> APtr = make_shared<AClass>();
	auto BPtr = make_shared<BClass>();
	APtr->SetB(BPtr);
	BPtr->SetA(APtr);
	*/
	/*
	unique_ptr<vector<shared_ptr<Test>>> VecPtr;
	VecPtr = Make();
	cout << "How many data points do you want to enter: ";
	int Num;
	cin >> Num;
	Fill(*VecPtr, Num);
	Display(*VecPtr);
	*/
	/*
	int Kilometers{};
	int Liters{};
	double KilometersPerLiter{};

	cout << "Enter the Kilometers: ";
	cin >> Kilometers;
	cout << "Enter the Liters: ";
	cin >> Liters;

	try
	{
		KilometersPerLiter = CalculateLpK(Kilometers, Liters);
		cout << "Result: " << KilometersPerLiter << endl;
	}
	catch (const DividedByZeroExeption& Ex)
	{
		cerr << "Sorry, can't divide by zero!" << endl;
	}
	catch (const NegativeValueExeption& Ex)
	{
		cerr << "Sorry, one of your paramteters is negative!" << endl;
	}
	catch (...)
	{
		cerr << "Unknown exception!" << endl;
	}

	cout << "Bye" << endl;
	*/
	/*
	unique_ptr<Account> MyAccount;

	try
	{
		MyAccount = make_unique<CheckingAccount>("My Account", 105.4);
		cout << *MyAccount << endl;
	}
	catch (const IllegalBalanceExeption& Ex)
	{
		cerr << Ex.what() << endl;
	}

	try
	{
		if (MyAccount == nullptr)
			throw NullPointerException{};

		MyAccount->Withdraw(500.0);
		cout << *MyAccount << endl;
	}
	catch (const InsufficientFundsException& Ex)
	{
		cerr << Ex.what() << endl;
	}
	catch (const NullPointerException& Ex)
	{
		cerr << Ex.what() << endl;
	}
	catch (...)
	{
		cerr << "Unknown exeption" << endl;
	}
	*/
	/*
	//int Int;
	//double Double;

	InFile.open("Responses.txt");
	if (!InFile)
	{
		cerr << "Couldn't open the file!" << endl;
		return 1;
	}

	//while (InFile >> Line >> Int >> Double)
	//{
	//	cout << setw(10) << left << Line
	//		<< setw(10) << Int
	//		<< setw(10) << Double
	//		<< endl;
	//}
	//string Line;
	//while (getline(InFile, Line))
	//{
	//	cout << Line << endl;
	//}
	//char c{};
	//while (InFile.get(c))
	//{
	//	cout << c;
	//}

	const int ROW_WIDTH_RIGHT{ 5 };
	const int ROW_WIDTH_LEFT{ 15 };
	const int TOTAL_WIDTH{ 20 };
	cout << setw(ROW_WIDTH_LEFT) << left << "Student"
		<< setw(ROW_WIDTH_RIGHT) << right << "Score"
		<< endl
		<< setw(TOTAL_WIDTH) << setfill('-') << ""
		<< endl
		<< setfill(' ') << "";

	InFile >> CorrectAnswers;

	string Name{};
	string Answers{};
	int StudentCount{ 0 };
	int Sum{ 0 };
	double Average{ 0.0 };

	while (!InFile.eof())
	{
		InFile >> Name >> Answers;
		int RightAnswers = CompareAnswers(Answers);
		cout << setw(15) << left << Name
			<< setw(5) << right << RightAnswers
			<< endl;
		StudentCount++;
		Sum += RightAnswers;
	}

	cout << setprecision(2);

	if (StudentCount != 0)
		Average = static_cast<double>(Sum) / StudentCount;

	cout << setw(TOTAL_WIDTH) << setfill('-') << "" << endl;
	cout  << setfill(' ') << ""
		<< setw(ROW_WIDTH_LEFT) << left << "Average score"
		<< setw(ROW_WIDTH_RIGHT) << right << Average
		<< endl;
	*/
	/*
	ifstream InFile;
	InFile.open("RomeoAndJuliet.txt");
	if (!InFile)
	{
		cerr << "Couldn't open the file!" << endl;
		return 1;
	}

	const string QUIT_APPLICATION{ "QUIT-APPLICATION" };

	FillWords(InFile);

	// close the file directly after it has been used
	InFile.close();

	// Ask the user for a word to search for in the Words vector
	string Word{};
	string WordUpper{};
	bool bQuit{ false };

	while (!bQuit)
	{
		bSearchAllUpper = AskForCapitalisation();

		cout << "[ Enter '" << QUIT_APPLICATION << "' to exit the application.]" << endl;
		cout << "Enter a word to search for: ";
		cin >> Word;
		WordUpper = Word;
		transform(WordUpper.begin(), WordUpper.end(), WordUpper.begin(), toupper);

		// exit application and break loop when the user types in the quitting string
		if (WordUpper == QUIT_APPLICATION)
		{
			bQuit = true;
			continue;
		}

		int WordCount;
		if (bSearchAllUpper)
			WordCount = SearchForWord(WordUpper, UpperLines);
		else
			WordCount = SearchForWord(Word, Lines);

		cout << "Found the word: " << WordUpper << " " << WordCount << " times." << endl;
	}
	*/
	/*
	ifstream InFile{ "RomeoAndJuliet.txt" };
	ofstream OutFile{ "Output.txt" };

	if (!InFile)
	{
		cerr << "Error opening InFile !" << endl;
		return 1;
	}
	if (!OutFile)
	{
		cerr << "Error opening OutFile!" << endl;
		return 1;
	}

	string Line;
	int LineNumber{ 0 };
	while (getline(InFile, Line))
	{
		if (Line != "")
		{
			LineNumber++;
			OutFile << setw(10) << left << LineNumber << Line << endl;
		}
		else
			OutFile << endl;

	}

	cout << "File copied successfully!" << endl;

	InFile.close();
	OutFile.close();
	*/
	/*
	int Int{};
	double Double{};
	string String{};

	string Example{ "General 42 99.987" };
	istringstream Iss{ Example };

	Iss >> String >> Int >> Double;
	cout << setw(10) << left << String
		<< setw(10) << left << Int
		<< setw(10) << right << Double
		<< endl;

	cout << endl << "------------------------------" << endl;

	ostringstream Oss{};
	Oss << setw(10) << left << String
		<< setw(10) << left << Int
		<< setw(10) << right << Double
		<< endl;
	cout << Oss.str() << endl;

	cout << endl << "------ Data  Validation ------" << endl;

	int Value{};
	string Entry{};
	bool done{ false };
	do
	{
		cout << "Please enter an integer: ";
		cin >> Entry;
		istringstream Ss{ Entry };
		if (Ss >> Value)
			done = true;
		else
			cout << "Sorry, that's not an integer" << endl;

		cin.ignore(numeric_limits<streamsize>::max(), '\n');

	} while (!done);
	*/
	/*
	Item<int> Item1{ "Hello", 42 };
	cout << Item1.GetName() << " " << Item1.GetValue() << endl;

	Item<string> Item2{ "There", "Icecream" };
	cout << Item2.GetName() << " " << Item2.GetValue() << endl;

	Item<Item<string>> Item3{ "Sword", {"Diamond", "Enhancement"} };
	cout << Item3.GetName() << " "
		<< Item3.GetValue().GetName() << " "
		<< Item3.GetValue().GetName()
		<< endl;
	*/
	/*
	Array<int, 5> Nums;
	cout << "The size of Nums is: " << Nums.GetSize() << endl;
	cout << Nums << endl;

	Nums.Fill(0);
	cout << "The size of Nums is: " << Nums.GetSize() << endl;
	cout << Nums << endl;

	Nums.Fill(10);
	cout << Nums << endl;

	Nums[1] = 100;
	Nums[2] = 500;
	cout << Nums << endl;

	Array<int, 100> Nums2{ 42 };
	cout << "The size of Nums is: " << Nums2.GetSize() << endl;
	cout << Nums2 << endl;

	Array<string, 15> Strings{ string{"My name"} };
	cout << "The size of Strings is: " << Strings.GetSize() << endl;
	cout << Strings << endl;

	Strings[8] = string{ "Hehehe" };
	cout << Strings << endl;

	Strings.Fill(string{ "Z" });
	cout << Strings << endl;
	*/
	/*
	vector<string> TestStrings{ "a", "aa", "aba", "abba", "abbcbba", "ab", "abc", "radar", "bob", "ana",
		"avid diva", "Amore Roma","A Toyota's a toyota", "C++", "A Santa at NASA",
		"A man, a plan, a cat, a ham, a yak, a yam, a hat, a canal-Panama!", "This is a palindrome", "palindrome" };

	cout << boolalpha;
	cout << setw(8) << left << "Result" << "String" << endl;

	for (const auto& S : TestStrings)
	{
		cout << setw(8) << left << IsPalindrome(S) << S << endl;
	}

	cout << endl;
	*/
	/*
	list<Song> Playlist
	{
		{"Hypa Hypa", "Eskimo Callboy", 10},
		{"Skyline Divided", "Seven", 6},
		{"Throne", "Bring Me The Horizon", 5},
		{"A Little Piece Of Heaven", "Avenged Sevenfold", 8},
		{"Suffer", "Kingdom Collapse", 7},
		{"Infectious", "Imminence", 8},
		{"Panic", "From Ashes To New", 9},
		{"Jekyll And Hyde", "Five Finger Death Punch", 10}
	};

	list<Song>::iterator CurrentSong = Playlist.begin();

	DisplayPlaylist(Playlist, *CurrentSong);

	while (!bQuit)
	{
		DisplayMenu();
		char Input;
		cin >> Input;
		cout << endl;
		//SwitchOverInput(Input, Playlist, CurrentSong);
		switch (Input)
		{
		case 'f':
		case 'F':
			CurrentSong = PlayFirstSong(Playlist);
			break;
		case 'l':
		case 'L':
			CurrentSong = PlayLastSong(Playlist);
			break;
		case 'n':
		case 'N':
			CurrentSong = PlayNextSong(Playlist, CurrentSong);
			break;
		case 'p':
		case 'P':
			CurrentSong = PlayPreviousSong(Playlist, CurrentSong);
			break;
		case 'a':
		case 'A':
			CurrentSong = AddNewSong(Playlist, CurrentSong);
			break;
		case 'd':
		case 'D':
			DisplayPlaylist(Playlist, *CurrentSong);
			break;
		case 'q':
		case 'Q':
			bQuit = true;
			break;
		default:
			break;
		}
	}
	*/
	/*
	DisplayWordCounts();
	DisplayWordOccurences();
	*/
#pragma endregion