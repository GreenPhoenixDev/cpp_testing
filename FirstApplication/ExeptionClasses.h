#pragma once

class DividedByZeroExeption
{
};

class NegativeValueExeption
{
};

class IllegalBalanceExeption : public std::exception
{
public:
	// Constructors
	IllegalBalanceExeption() noexcept = default;
	// Destructors
	~IllegalBalanceExeption() = default;

	virtual const char* what() const noexcept { return "Illegal balance exception!"; }
};

class InsufficientFundsException : public std::exception
{
public:
	InsufficientFundsException() noexcept = default;	
	// Destructors
	~InsufficientFundsException() = default;

	virtual const char* what() const noexcept { return "Insufficient funds exception!"; }
};

class NullPointerException : public std::exception
{
public:
	NullPointerException() noexcept = default;
	// Destructors
	~NullPointerException() = default;

	virtual const char* what() const noexcept { return "Null Pointer exception!"; }
};