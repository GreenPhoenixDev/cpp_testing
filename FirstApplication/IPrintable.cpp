#include "IPrintable.h"

ostream& operator<<(ostream& Os, const IPrintable& obj)
{
	obj.Print(Os);

	return Os;
}