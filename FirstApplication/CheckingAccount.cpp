#include "CheckingAccount.h"

// Overloaded constructor
CheckingAccount::CheckingAccount(string InName, double InBalance, double InFee)
	: Account{InName, InBalance}, Fee{InFee} {}

bool CheckingAccount::Withdraw(double InAmount)
{
	InAmount += Fee;

	return Account::Withdraw(InAmount);
}

void CheckingAccount::Print(ostream& Os) const
{
	Os << "[ Checking account: " << Name << " : " << Balance << " , " << Fee << "$ ]";
}