#include "SavingsAccount.h"

// Overloaded constructor
SavingsAccount::SavingsAccount(string InName, double InBalance, double InInterest)
	: Account{InName, InBalance}, InterestRate{InInterest} {}

bool SavingsAccount::Deposit(double InAmount)
{
	InAmount += InAmount * (InterestRate/100);

	return Account::Deposit(InAmount);
}

void SavingsAccount::Print(ostream& Os) const
{
	Os << "[ Savings account: " << Name << " : " << Balance << " , " << InterestRate << "% ]";
}