#pragma once

#include <string>

using std::string;

template <typename T>
class Item
{
private:
	string Name;
	T Value;

public:
	Item(string InName, T InValue) : Name{InName}, Value{InValue} {}
	string GetName() const { return Name; }
	T GetValue() const { return Value; }
};