#include "Song.h"
#include <iomanip>

using std::setw;
using std::left;

ostream& operator<<(ostream& Os, const Song& S)
{
	Os << setw(30) << left << S.Name
		<< setw(40) << left << S.Artist
		<< setw(2) << left << S.Rating;

	return Os;
}