#pragma once

#include <iostream>
#include <string>

using std::ostream;
using std::string;

class Song
{
	friend ostream& operator<<(ostream& Os, const Song& S);

private:
	string Name;
	string Artist;
	int Rating;

public:
	Song() = default;
	Song(string InName, string InArtist, int InRating)
		: Name{InName}, Artist{InArtist}, Rating{InRating} {}

	string GetName() const { return Name; }
	string GetArtist() const { return Artist; }
	int GetRating() const { return Rating; }

	bool operator<(const Song& Rhs) const { return this->Name < Rhs.Name; }
	bool operator==(const Song& Rhs) const { return this->Name == Rhs.Name; }
};