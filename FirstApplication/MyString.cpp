#include <iostream>
#include <string>
#include "MyString.h"

// No-arguments constructor
MyString::MyString()
	: Str{ nullptr }
{
	Str = new string{ "" };
}

// Overloaded constructor
MyString::MyString(const string* Source)
	: Str{ nullptr }
{
	if (Source == nullptr) { Str = new string{ "" }; }
	else { Str = new string{ *Source }; }
}

//Copy constructor
MyString::MyString(const MyString& Source)
	: Str{ nullptr }
{
	Str = new string{ *Source.Str };
}

// Move constructor
MyString::MyString(MyString&& Source) noexcept
	: Str{ Source.Str }
{
	Source.Str = nullptr;
}

// Destructor
MyString::~MyString()
{
	delete Str;
}

// Copy assignment
MyString& MyString::operator=(const MyString& Rhs)
{
	if (this == &Rhs) { return *this; }

	delete Str;
	Str = new string{ *Rhs.Str };
	return *this;
}

// Move assignment
MyString& MyString::operator=(MyString&& Rhs) noexcept
{
	if (this == &Rhs) { return *this; }

	delete Str;
	Str = Rhs.Str;
	Rhs.Str = nullptr;
	return *this;
}

// Equality check
bool MyString::operator==(const MyString& Rhs) const
{
	if (this == &Rhs) { return true; }

	if (this->Str->compare(*Rhs.Str) == 0)
		return true;
	else
		return false;
}

// Inequality check
bool MyString::operator!=(const MyString& Rhs) const
{
	if (this == &Rhs) { return false; }

	if (!this->Str->compare(*Rhs.Str) == 0)
		return true;
	else
		return false;
}

// Lexical check - less
bool MyString::operator<(const MyString& Rhs) const
{
	return true;
}

// Lexical check - greater
bool MyString::operator>(const MyString& Rhs) const
{
	return true;
}