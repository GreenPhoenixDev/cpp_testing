#include "Movie.h"
#include <iostream>

using std::endl;
using std::cout;

void Movie::SetWatchCount(int Value)
{ 
	WatchCount += Value; 
	cout << Name << " has now a watchcount of " << WatchCount << " times." << endl;
	cout << endl;
}


Movie::Movie(string MovieName, string RatingName, int WatchedValue)
	: Name{ MovieName }, Rating{ RatingName }, WatchCount{ WatchedValue } { cout << "Movie Constructor called" << endl; }