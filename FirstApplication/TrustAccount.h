#pragma once

#include "SavingsAccount.h"

class TrustAccount : public SavingsAccount
{
public:
	virtual void Print(ostream& Os) const override;
	// Constructor
	TrustAccount(string InName = DEFAULT_NAME, double InBalance = DEFAULT_BALANCE, double InInterest = DEFAULT_INTEREST_RATE);
	// Destructor
	virtual ~TrustAccount(){ cout << "Trust Account destructor" << endl; }

	bool Deposit(double InAmount);
	bool Withdraw(double InAmount);

protected:
	int Withdrawals;

private:
	static constexpr const char* DEFAULT_NAME = "Unnamed Trust Account";
	static constexpr double	DEFAULT_BONUS_DEPOSIT = 50.0;
	static constexpr double	DEFAULT_MIN_VALUE_FOR_BONUS_DEPOSIT = 50000.0;
	static constexpr double	DEFAULT_MAX_WITHDRAWAL_PERCENT = 20;
	static constexpr int DEFAULT_MAX_WITHDRAWALS = 3;
};