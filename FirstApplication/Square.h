#pragma once

#include "ClosedShape.h"

using std::cout;
using std::endl;

class Square : public ClosedShape
{
public:
	virtual ~Square() {};

	virtual void Draw() override { cout << "Drawing a square" << endl; }
	virtual void Rotate() override { cout << "Rotating a square" << endl; }
};