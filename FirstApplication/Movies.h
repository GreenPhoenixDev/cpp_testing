#pragma once

#include <iostream>
#include <vector>
#include <string>
#include "Movie.h"

using std::vector;
using std::string;
using std::cout;
using std::cin;
using std::endl;

class Movies
{
public:
	void DefaultUpdate();

	bool bIsRunning{ true };
private:

	void DisplayMenu() const;
	void SwitchOverInput();
	void DisplayMovieList() const;
	void AddMovie();
	void AddWatchCountOne();
	void AddWatchCount();
	Movie* GetMovieWithName(const string& MovieName);
	bool ValidateMovie(const string& MovieName, const string& RatingName, int Value) const;
	bool CheckIfNameExists(const string& MovieName) const;
	bool CheckIfRatingIsCorrect(const string& RatingName) const;
	bool CheckIfIsPositive(int Value) const;
	void QuitApplication();
	void DeconstructMovies();

	vector<Movie*> MovieList;
	vector<string> Ratings{ "G", "PG", "PG-13", "R" };
	char Command{ ' ' };
};