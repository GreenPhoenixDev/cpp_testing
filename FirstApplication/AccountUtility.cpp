#include "AccountUtility.h"
#include <iostream>

using std::cout;
using std::endl;

// Accounts

void Display(const vector<Account*>& InAccounts)
{
	cout << endl;
	cout << "================================ Accounts ================================" << endl;
	for (const auto Acc : InAccounts)
	{
		cout << *Acc << endl;
	}
}

void Deposit(vector<Account*>& InAccounts, double InAmount)
{
	cout << endl;
	cout << "========================= Depositing to Accounts =========================" << endl;
	for (auto Acc : InAccounts)
	{
		if (Acc->Deposit(InAmount)) { cout << "Deposited " << InAmount << " to " << *Acc << endl; }
		else { cout << "Failed Deposit of " << InAmount << " to " << *Acc << endl; }
	}
}

void Withdraw(vector<Account*>& InAccounts, double InAmount)
{
	cout << endl;
	cout << "======================== Withdrawing from Accounts =======================" << endl;
	for (auto Acc : InAccounts)
	{
		if (Acc->Withdraw(InAmount)) { cout << "Withdrew " << InAmount << " from " << *Acc << endl; }
		else { cout << "Failed Withdrawal of " << InAmount << " from " << *Acc << endl; }
	}
}