#pragma once

#include "Account.h"

class CheckingAccount : public Account
{
public:
	virtual void Print(ostream& Os) const override;
	// Constructor
	CheckingAccount(string InName = DEFAULT_NAME, double InBalance = DEFAULT_BALANCE, double InFee = DEFAULT_FEE);
	// Destructor
	virtual ~CheckingAccount() { cout << "Checking Account destructor" << endl; }

	virtual bool Deposit(double InAmount) override { return Account::Deposit(InAmount); }
	virtual bool Withdraw(double InAmount) override;

protected:
	double Fee;

private:
	static constexpr const char* DEFAULT_NAME = "Unnamed Savings Account";
	static constexpr double DEFAULT_BALANCE = 0.0;
	static constexpr double DEFAULT_FEE = 1.5;
};