#pragma once

#include <vector>
#include "Account.h"
#include "SavingsAccount.h"
#include "CheckingAccount.h"
#include "TrustAccount.h"

using std::vector;

void Display(const vector<Account*>& InAccounts);
void Deposit(vector<Account*>& InAccounts, double InAmount);
void Withdraw(vector<Account*>& InAccounts, double InAmount);